-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2019-12-18 03:55:36.753

-- tables
-- Table: Fotos
CREATE TABLE Fotos (
    fotosID int NOT NULL,
    path varchar(40) NOT NULL,
    CONSTRAINT Fotos_pk PRIMARY KEY (fotosID)
);

-- Table: Propiedades
CREATE TABLE Propiedades (
    propiedadesID int NOT NULL,
    ubicacion varchar(60) NULL,
    precio real(5,1) NOT NULL,
    CONSTRAINT Propiedades_pk PRIMARY KEY (propiedadesID)
);

-- Table: Usuarios
CREATE TABLE Usuarios (
    usuarioID int NOT NULL,
    nombre varchar(40) NULL,
    telefono varchar(15) NULL,
    correo varchar(50) NULL,
    constrasenia varchar(50) NULL,
    CONSTRAINT Usuarios_pk PRIMARY KEY (usuarioID)
);

-- Table: propiedadesFotos
CREATE TABLE propiedadesFotos (
    propiedadesFotosID int NOT NULL,
    propiedadesID int NOT NULL,
    fotosID int NOT NULL,
    CONSTRAINT propiedadesFotos_pk PRIMARY KEY (propiedadesFotosID)
);

-- Table: usuariosPropiedades
CREATE TABLE usuariosPropiedades (
    usuariosPropiedadesID int NOT NULL,
    usuarioID int NOT NULL,
    propiedadesID int NOT NULL,
    CONSTRAINT usuariosPropiedades_pk PRIMARY KEY (usuariosPropiedadesID)
);

-- foreign keys
-- Reference: propiedadesFotos_Fotos (table: propiedadesFotos)
ALTER TABLE propiedadesFotos ADD CONSTRAINT propiedadesFotos_Fotos FOREIGN KEY propiedadesFotos_Fotos (fotosID)
    REFERENCES Fotos (fotosID);

-- Reference: propiedadesFotos_Propiedades (table: propiedadesFotos)
ALTER TABLE propiedadesFotos ADD CONSTRAINT propiedadesFotos_Propiedades FOREIGN KEY propiedadesFotos_Propiedades (propiedadesID)
    REFERENCES Propiedades (propiedadesID);

-- Reference: usuariosPropiedades_Propiedades (table: usuariosPropiedades)
ALTER TABLE usuariosPropiedades ADD CONSTRAINT usuariosPropiedades_Propiedades FOREIGN KEY usuariosPropiedades_Propiedades (propiedadesID)
    REFERENCES Propiedades (propiedadesID);

-- Reference: usuariosPropiedades_Usuarios (table: usuariosPropiedades)
ALTER TABLE usuariosPropiedades ADD CONSTRAINT usuariosPropiedades_Usuarios FOREIGN KEY usuariosPropiedades_Usuarios (usuarioID)
    REFERENCES Usuarios (usuarioID);

-- End of file.

