<?php
	if($_POST) {
		if((!empty($_POST["correo"])) && (!empty($_POST["contra"]))) {
			include("funciones.php");
			$correo = $_POST["correo"];
			$contra = $_POST["contra"];
	   	$ingreso = ingresarSesion($correo,$contra);

	   	if($ingreso == -1) {
				echo "<script>window.alert('Usuario o contraseña incorrectos');</script>";
	   	}else {
				session_start();

				$_SESSION['id'] = $ingreso[0];
				$_SESSION['nombre'] = $ingreso[1];
				$_SESSION['apellidoPaterno'] = $ingreso[2];
				$_SESSION['apellidoMaterno'] = $ingreso[3];
				$_SESSION['tipoLetra'] = $ingreso[5];
				$_SESSION['estiloFondo'] = $ingreso[6];
				$_SESSION['correo'] = $ingreso[7];
				header("location: pagPrincipalInvestigador.php");
	   	}
	   }

	}
?>
<html>
	<head>
		<meta charset="UTF-8" />
		<link rel="stylesheet" href="../bootstrap/css/bootstrap.css">
		<title>Inicio</title>
		<style type="text/css">
		 	fieldset.scheduler-border {
    			border: 1px groove #ddd !important;
    			padding: 0 1.4em 1.4em 1.4em !important;
    			margin: 0 0 1.5em 0 !important;
    			-webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
			}
			legend.scheduler-border {
        		font-size: 1.2em !important;
        		font-weight: bold !important;
        		text-align: left !important;
        		width:auto;
        		padding:0 10px;
        		border-bottom:none;
    		}
    	</style>
	</head>
	<body>
		<div id="divPrincipal" style="position: absolute; left: 5vh; top: 5vh;">
			<a href="index.php" style="font-size:35px;width:100px; height:40px; "><img src="imagenes/left.png" style="height:90px; width:90px;"></a>
		</div>
		<div style="position: relative; top: 10vh; left:65vh; width:99vh; height:18vh;">
			<a href="registroInvestigadores.php" class="btn btn-primary btn-lg btn-block">Regístrate</a>
		</div>
		<div style="position:relative;">
			<hr>
		</div>
		<div style="position:relative; top:2vh; left:65vh; width: 99vh;">

			<form action="ingresoInvestigadores.php" method="POST" >
				<fieldset class="scheduler-border">
				<legend class="scheduler-border">¿Tienes cuenta? Inicia sesión</legend>
					<label class="control-label input-label" for="correo" style="font-size: 25px;">Correo electrónico</label>
					<input type="email" placeholder="Ejemplo@.com" name="correo" id="correo" required  class="form-control"/>
					<label class="control-label input-label" for="correo" style="font-size: 25px;">Contraseña</label>
					<input type="password" name="contra" id="contra" required  class="form-control"/>
					<div style="position: relative; left:75vh; top:1vh; ">
						<input type="submit" value="Iniciar sesión" name="iniciarSesion" class="btn btn-success" style="border-radius:10%;" >
					</div>
					<div style="position:relative;">
						<p id="estadoValidacion" style="color: red;"></p>
					</div>
				</form>
			</fieldset>
		</div>
	</body>
</html>
